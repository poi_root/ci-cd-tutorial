import pytest

from ..divide import divide


@pytest.mark.parametrize(
    "x, y, expected",
    [(2, 2, 1), (5, 2, 2.5)]
)
def test_divide(x, y, expected):
    assert divide(x, y) == expected
